/** 

DOMINIC NIGHTINGALE
MAE 144
BEWLEY
HOMEWORK 4

*****************************************************************
 PART A
 
 I CAN SEE HOW THE LIGHT IS FLASHING AT DIFFERENT RATES AS WELL AS
 THE TIME THAT THE LIGHT IS ACTULLY ON HENCE THE PWM FREQUENCY AND 
 THE DUTY CYCLES RESPECTIVELY.

*****************************************************************
 PART B
 
 DRIVING EACH LED INDEPENDTLY WAS VERY INTERESTING TO WATCH AND 
 WHILE LOOKING AT HTOP, I GOT TO SEE ALL THE THREADS BE ACTIVE. 
 I FOUND IT STRANGE THAT THESE PROCESSES WERE TAKING 100% OF THE 
 CPU. 

*****************************************************************

*/




#include <rc/button.h>
#include <rc/led.h>
#include <rc/start_stop.h>
#include <rc/time.h>
#include <rc/pthread.h>
#include <time.h>
#include <stdio.h> 
#include <signal.h>
#include <pthread.h>
#include <unistd.h>

#define QUIT_TIMEOUT_US 1500000 // quit after 1.5 seconds holding pause button
#define QUIT_CHECK_US   100000  // check every 1/10 second
#define NUM_THREADS 4
#define POLICY SCHED_FIFO
static int mode;
int a;
int signal_number;
int count;
char y;
char n;
char init_restart;
double duty1;
double duty2;
double dutyHigh1 = 0.95;
double dutyLow1;
double dutyHigh2 = 0.65;
double dutyLow2;
double dutyHigh3 = 0.05;
double dutyLow1;
double freq; //frequency in hertz
double sleepTime;
double freq1 = 0.5; // 1/s
double freq2 = 200.0; // 1/s
// double freq3 = 1000.0; // 1/s
// double freq4 = 10000.0; // 1/s
double freq3 = 1.0; // 1/s
double freq4 = 5.0; // 1/s
int endwait = 10; // wait 10sec
int greenLED = 0;
int redLED = 1;
int blueLED0 = 2;
int blueLED1 = 3;
time_t start_time;
time_t elappsed_time;
time_t new_time;
static int running = 1;
static int thread_ret_val;
static pthread_t thread[NUM_THREADS];
int one_second = 1000000;


static void __on_mode_release(void) {
        if(mode<5) mode++; 
        else mode=0;
        printf("mode: %d\n",mode);      
        return;
}


void sigintHandler(int sig_num) 
{ 
    printf("\nCtrl+C detected, begin exiting main while loop\n"); 
    signal_number = sig_num;
    running = 0;
    rc_set_state(EXITING);
    return;
} 

void blinking(double duty1, double freq, int led_val) { 
        signal(SIGINT, sigintHandler);
        // printf("got to top...\n");
        duty2 = 1.0 - duty1;
	sleepTime = 1000000.0 / freq;
        while (running) {
                rc_led_set(led_val,1);
    		rc_usleep(duty1*sleepTime);
                rc_led_set(led_val,0);
    		rc_usleep(duty2*sleepTime);
        }
        // printf("got to bottom...\n");                             
}

void* __THREAD_1_GREEN(void* arg) { 

        int received_argument = *(int*)arg;
        if (mode == 0 ) {duty1 = dutyHigh1; freq = freq3;}
        else if (mode == 1 ) {duty1 = dutyHigh2; freq = freq3;}
        else if (mode == 2 ) {duty1 = dutyHigh3; freq = freq3;}
        else if (mode == 3 ) {duty1 = dutyHigh1; freq = freq4;}
        else if (mode == 4 ) {duty1 = dutyHigh2; freq = freq4;}
        else if (mode == 5 ) {duty1 = dutyHigh3; freq = freq4;}

        while(running) {
                blinking(duty1,freq,greenLED);
        }
        printf("Status: greenLED %d\n", greenLED);

        thread_ret_val=received_argument;
        return (void*)&thread_ret_val;
}

void* __THREAD_2_RED(void* arg) { 

        int received_argument = *(int*)arg;
        if (mode == 0 ) {duty1 = dutyHigh1; freq = freq3;}
        else if (mode == 1 ) {duty1 = dutyHigh2; freq = freq3;}
        else if (mode == 2 ) {duty1 = dutyHigh3; freq = freq3;}
        else if (mode == 3 ) {duty1 = dutyHigh1; freq = freq4;}
        else if (mode == 4 ) {duty1 = dutyHigh2; freq = freq4;}
        else if (mode == 5 ) {duty1 = dutyHigh3; freq = freq4;}

        while(running) {
                blinking(duty1,freq,redLED);
        }
        printf("Status: redLED %d\n", redLED);

        thread_ret_val=received_argument;
        return (void*)&thread_ret_val;
}

void* __THREAD_3_BLUE_0(void* arg) { 

        int received_argument = *(int*)arg;
        if (mode == 0 ) {duty1 = dutyHigh1; freq = freq3;}
        else if (mode == 1 ) {duty1 = dutyHigh2; freq = freq3;}
        else if (mode == 2 ) {duty1 = dutyHigh3; freq = freq3;}
        else if (mode == 3 ) {duty1 = dutyHigh1; freq = freq4;}
        else if (mode == 4 ) {duty1 = dutyHigh2; freq = freq4;}
        else if (mode == 5 ) {duty1 = dutyHigh3; freq = freq4;}

        while(running) {
                blinking(duty1,freq,blueLED0);
        }
        printf("Status: blueLED0 %d\n", blueLED0);

        thread_ret_val=received_argument;
        return (void*)&thread_ret_val;
}

void* __THREAD_4_BLUE_1(void* arg) { 

        int received_argument = *(int*)arg;
        if (mode == 0 ) {duty1 = dutyHigh1; freq = freq3;}
        else if (mode == 1 ) {duty1 = dutyHigh2; freq = freq3;}
        else if (mode == 2 ) {duty1 = dutyHigh3; freq = freq3;}
        else if (mode == 3 ) {duty1 = dutyHigh1; freq = freq4;}
        else if (mode == 4 ) {duty1 = dutyHigh2; freq = freq4;}
        else if (mode == 5 ) {duty1 = dutyHigh3; freq = freq4;}

        while(running) {
                blinking(duty1,freq,blueLED1);
        }
        printf("Status: blueLED1 %d\n", blueLED1);

        thread_ret_val=received_argument;
        return (void*)&thread_ret_val;
}

int main() {
        signal(SIGINT, sigintHandler);
        // make PID file to indicate your project is running
        rc_make_pid_file();
        // prepare to run
        rc_set_state(RUNNING);
        mode = 0;
        printf("\nPress mode to change problem\n");
        printf("hold pause button to exit\n");
        int arg = 0;
        int policy = POLICY;
        int priority1 = 10;
        int priority2 = 20;
        int priority3 = 30;
        int priority4 = 40;

        
        // initialize pause and mode buttons
        if(rc_button_init(RC_BTN_PIN_PAUSE, RC_BTN_POLARITY_NORM_HIGH,RC_BTN_DEBOUNCE_DEFAULT_US)){
                fprintf(stderr,"ERROR: failed to init buttons\n");
                return -1;
        }
        if(rc_button_init(RC_BTN_PIN_MODE, RC_BTN_POLARITY_NORM_HIGH,RC_BTN_DEBOUNCE_DEFAULT_US)){
                fprintf(stderr,"ERROR: failed to init buttons\n");
                return -1;
        }
        // Assign functions to be called when button events occur
        rc_button_set_callbacks(RC_BTN_PIN_MODE, NULL, __on_mode_release);
        
        // thank you Joon!
        arg = 1;
        if(rc_pthread_create(&thread[0],__THREAD_1_GREEN,(void*)&arg, policy, priority1)==-1)
                printf("Failed to start thread");
        else printf("...Good\n");
        
        arg = 2;
        if(rc_pthread_create(&thread[1],__THREAD_2_RED,(void*)&arg, policy, priority2)==-1)
                printf("Failed to start thread");
        else printf("...Good\n");

        arg = 3;
        if(rc_pthread_create(&thread[2],__THREAD_3_BLUE_0,(void*)&arg, policy, priority3)==-1)
                printf("Failed to start thread");
        else printf("...Good\n");

        arg = 4;
        if(rc_pthread_create(&thread[3],__THREAD_4_BLUE_1,(void*)&arg, policy, priority4)==-1)
                printf("Failed to start thread");
        else printf("...Good\n");
        

        while (running) {
        }
        
        rc_led_set(RC_LED_GREEN, 0);
        rc_led_set(RC_LED_RED, 0);
        rc_led_cleanup();
        rc_button_cleanup();
        rc_remove_pid_file();
        printf("\nGoodbye World!\n");
        return 0;
}
