/** 

DOMINIC NIGHTINGALE
MAE 144
BEWLEY
HOMEWORK 4

*****************************************************************
 PART A
 ACCELEROMETER: 
   UNITS: m/s^2
   RANGE: ACCEL_FSR_2G, ACCEL_FSR_4G, ACCEL_FSR_8G, ACCEL_FSR_16G 
 GYRO: 
   UNITS: deg/s
   RANGE:GYRO_FSR_250DPS, GYRO_FSR_500DPS, GYRO_FSR_1000DPS, GYRO_FSR_2000DPS 
*****************************************************************
 PART B
 need the following:
     use the following functions and the accelerometer to primt the angle 
     (in radians) of the BBB relative to the direction of gravity about
     its X-axis.
     functions:
         rc mpu default config()
         int rc mpu initialize (rc mpu data t *data, rc mpu config t conf)
         int rc mpu power off()
         int rc mpu read accel (rc mpu data t *data)
         int rc mpu read gyro (rc mpu data t *data)

 The angle estimate should:
     It should only use Y and Z data
     use rc_usleep() at the end of the loop to set a 100hz refresh rate
*****************************************************************
 PART C

 I used a frequency of 10hz becuase whil working on the project I 
 actually was able to stablize the MIP on the same order of magnitute 
 of time so I figured this would be a good starting point. This could
 be imporved upon implementaion of the actual MIP however.
 
 NOTE:
 high and low pass filters made in seperate document 
 			"filters_for_problem_3.pdf"

*****************************************************************
 PART D

 After plotting in matlab, it looks like theta_g is not quite right 
 but has the same representative shape as theta_a which makes me think
 it might have been calibrated incorrectly or maybe, an issue with the
 gyro, or most likely a typo in my code. In the plot, you can see how 
 the orientation is changing with time in a harmonic way becuase of the 
 manevours that were asked.

*****************************************************************
*/

#include <stdio.h>
#include <robotcontrol.h> // includes ALL Robot Control subsystems

// function declarations
void on_pause_press();
void on_pause_release();
rc_mpu_config_t conf;
rc_mpu_data_t data;
double theta_a_raw;
double x_accel;
double y_accel;
double z_accel;

double x_gyro;
double y_gyro;
double z_gyro;
int h;


int main()
{
	
	if(rc_kill_existing_process(2.0)<-2) return -1;

	// start signal handler so we can exit cleanly
	if(rc_enable_signal_handler()==-1){
		fprintf(stderr,"ERROR: failed to start signal handler\n");
		return -1;
	}

	rc_make_pid_file();
	
	conf = rc_mpu_default_config();
    rc_mpu_initialize(&data, conf);
	rc_set_state(RUNNING);
	h = 10000;
	double x_gyro_array[2];
	double y_accel_array[2];
	double z_accel_array[2];
	double theta_a_raw_array[2];
	double theta_g_raw_array[3];
	double theta_f;
	double low_pass_filter;
	double high_pass_filter;
	theta_g_raw_array[0] = 0.0;
	low_pass_filter = 0;
	high_pass_filter =0;
	FILE *output = fopen("output.csv","w+");
	
	while(rc_get_state()!=EXITING){
        rc_mpu_read_accel(&data);
        rc_mpu_read_gyro(&data);
		for (int counter=0; counter<=1; counter++) {
			x_accel = data.accel[0];
			y_accel = data.accel[1];
			z_accel = data.accel[2];
			x_gyro = data.gyro[0];
			y_gyro = data.gyro[1];
			z_gyro = data.gyro[2];
			y_accel_array[counter] = y_accel;
			z_accel_array[counter] = z_accel;
			x_gyro_array[counter] = x_gyro;
			theta_a_raw_array[counter] = atan2(-1*z_accel_array[counter], y_accel_array[counter]);
			theta_g_raw_array[0] = (x_gyro_array[counter])*DEG_TO_RAD*0.01;
			theta_g_raw_array[counter+1] = theta_g_raw_array[counter] + (x_gyro_array[counter])*DEG_TO_RAD*0.01;
			rc_usleep(h*10);
		}
		// high and low pass filters made in seperate document "filters_for_problem_3.pdf"
		low_pass_filter =  0.99*low_pass_filter + 0.0049752*(theta_a_raw_array[1] + theta_a_raw_array[0]);
		high_pass_filter = 0.99*high_pass_filter + (1-0.0049752)*(theta_g_raw_array[2] - theta_g_raw_array[1]);

		theta_f = low_pass_filter + high_pass_filter;
		printf("\rtheta a raw | %6.2f |theta_g_raw |%6.2f |filtered |%6.2f", theta_a_raw_array[1],theta_g_raw_array[2], high_pass_filter);
		fprintf(output,"%6.2f, %6.2f, %6.2f\n",low_pass_filter,high_pass_filter, theta_f);
		fflush(stdout);
	} 

	
	rc_mpu_power_off();
	rc_remove_pid_file();
	printf("everything has shut down \n");
	fclose(output);
	printf("ecsv file created \n");
	return 0;
}
