/*

DOMINIC NIGHTINGALE
MAE 144
BEWLEY
HOMEWORK 4

*/

#include <stdio.h>
#include <rc/button.h>
#include <rc/led.h>
#include <rc/start_stop.h>
#include <rc/time.h>
#include <stdio.h> 
#include <signal.h>
#define QUIT_TIMEOUT_US 1500000 // quit after 1.5 seconds holding pause button
#define QUIT_CHECK_US   100000  // check every 1/10 second
static const int us_delay[] = {400000, 170000, 100000};
static int mode;
static int toggle; // toggles between 0&1 for led blink
int a;
int signal_number;
int count;
char y;
char n;
char init_restart;
int led_val;
char power1;
char power2;
const int numDots = 4;

static void __on_pause_release(void)
{
        // toggle betewen paused and running modes
        if(rc_get_state()==RUNNING){
                rc_set_state(PAUSED);
                printf("PAUSED\n");
        }
        else if(rc_get_state()==PAUSED){
                rc_set_state(RUNNING);
                // printf("RUNNING\n");
        }
        fflush(stdout);
        return;
}

static void __on_pause_press(void)
{
        int i=0;
        const int samples = QUIT_TIMEOUT_US/QUIT_CHECK_US;
        // now keep checking to see if the button is still held down
        for(i=0;i<samples;i++){
                rc_usleep(QUIT_CHECK_US);
                if(rc_button_get_state(RC_BTN_PIN_PAUSE)==RC_BTN_STATE_RELEASED){
                        return;
                }
        }
        printf("long press detected, shutting down\n");
        rc_set_state(EXITING);
        return;
}

static void __on_mode_release(void)
{
        if(mode<2) {
                mode++;
                count=0;
        }      
        else {
                mode=0;
                // printf("You have cycled through each mode! Would you like to restart from the beginning? [y/n]: ");
                // scanf("%c",&init_restart);
                // if (init_restart == 'y') {
                //         count=0;
                //         printf("reloading...\n");
                }  
                else if (init_restart == 'n') {}
        }           
        return;
}

void sigintHandler(int sig_num) { 
    printf("\nCtrl+C detected, begin exiting main while loop\n"); 
    signal_number = sig_num;
    a=-1;
} 

void printRunningStatus(void) {
        while(rc_get_state()!=EXITING || signal_number !=2) {
                signal(SIGINT, sigintHandler);
                printf("\rStatus: Running");
                for (int i = 0; i < numDots; i++) {
                fputc('.', stdout);
                fflush(stdout);
                rc_usleep(1000000);
                }
        }
        

}
int main()
{
        if(rc_kill_existing_process(2.0)<-2) return -1;

        if(rc_enable_signal_handler()<0){
                rc_disable_signal_handler();
                fprintf(stderr,"ERROR: failed to complete rc_enable_signal_handler\n");
                return -1;
        }
        // initialize pause and mode buttons
        if(rc_button_init(RC_BTN_PIN_PAUSE, RC_BTN_POLARITY_NORM_HIGH,
                                                RC_BTN_DEBOUNCE_DEFAULT_US)){
                fprintf(stderr,"ERROR: failed to init buttons\n");
                return -1;
        }
        if(rc_button_init(RC_BTN_PIN_MODE, RC_BTN_POLARITY_NORM_HIGH,
                                                RC_BTN_DEBOUNCE_DEFAULT_US)){
                fprintf(stderr,"ERROR: failed to init buttons\n");
                return -1;
        }
        // Assign functions to be called when button events occur
        rc_button_set_callbacks(RC_BTN_PIN_PAUSE, __on_pause_press, __on_pause_release);
        rc_button_set_callbacks(RC_BTN_PIN_MODE, NULL, __on_mode_release);
        // start with both LEDs off
        if(rc_led_set(RC_LED_GREEN, 0)==-1){
                fprintf(stderr, "ERROR in rc_blink, failed to set RC_LED_GREEN\n");
                return -1;
        }
        if(rc_led_set(RC_LED_RED, 0)==-1){
                fprintf(stderr, "ERROR in rc_blink, failed to set RC_LED_RED\n");
                return -1;
        }
        rc_make_pid_file();
        rc_set_state(RUNNING);
        mode = 0;
        printf("\nPress mode to change blink rate\n");
        printf("hold pause button to exit\n");
        printRunningStatus();
        a = rc_enable_signal_handler();
        
        while(rc_get_state()!=EXITING || signal_number !=2) {
                signal(SIGINT, sigintHandler);
                if(signal_number ==2) break;
                else if(rc_get_state()==PAUSED) {}
                else if(rc_get_state()==RUNNING){
                        if (mode == 0) {led_val=0; power1=1; power2=0;}
                        else if(mode ==1) {led_val=1; power1=1; power2=1;}
                        if (count < 10) {
                                printf("setting mode: %d\n", mode);
                                for(count= 0; count<10;count++) {
                                        if(toggle){
                                                rc_led_set(led_val,power1);
                                                toggle = 0;
                                        }
                                        else{
                                                rc_led_set(led_val,power2);
                                                toggle=1;
                                        }
                                        rc_usleep(us_delay[mode]);
                                        
                                        
                                }
                                rc_led_set(led_val,0);
                                // printf("\nrestart? [y/n]: ");
                                // scanf("%c",&init_restart);
                                // printf("%d\n", count);
                                if (count == 10) printf("\nThis mode is finished! Check out the next one!\n");        
                        }
                        
                        // if (init_restart == 'y') {
                        //         count=0;
                        //         printf("reloading...\n");
                        // }  
                        // else if (init_restart == 'n') {}
                        // rc_usleep(us_delay[mode]);
                }
                rc_usleep(us_delay[mode]);
                
                // sleep the right delay based on current mode.
        }

        printf("\nYou have now exited the main while loop\n");
        // now that the while loop has exited, clean up neatly and exit compeltely.
        rc_led_set(RC_LED_GREEN, 0);
        rc_led_set(RC_LED_RED, 0);
        rc_led_cleanup();
        rc_button_cleanup();
        rc_remove_pid_file();
        printf("\nGoodbye World!\n");
        return 0;
        
}
